function buscarConjQuery( jsonParam ) {
    
    var link = "https://api.mercadolibre.com/sites/MLA/search";
    var query = $("#query").val();
    var params = { "q": query }
    params = $.extend(params,jsonParam);
    
    $.get( link, params, function( data ) {
        
        //se guardan las listas de Publicaciones resultantes, Tipo de orden y filtros disponibles
        var resultados = data.results;
        var ordenDisponibles = data.available_sorts;
        var filtrosDisponibles = data.available_filters;
        var filtrasAplicados = data.filters;
        
        //se llama a los metodos para listar los resultados, botones de ordenamiento y filtrado
        listarResultados(resultados);
        listarOrdenDisponibles(ordenDisponibles);
        listarFiltrosDisponibles(filtrosDisponibles, filtrasAplicados);
        
    });
    
}


//Mostras lista de productos resultantes
function listarResultados(listRes){
    
    var lista = $("<ul> </>").attr("id","elementos");
    //por cada publicacion que obtuvimos del request se imprime en el html
    for (var i = 0; i < listRes.length; i++) {
        var elem = $("<li></>");
        var nuevoA = $("<a></>").attr("href",listRes[i].permalink);
        nuevoA.append(listRes[i].title);
        
        elem.html(nuevoA);
        elem.append("  -->   Precio: $ ");
        elem.append(listRes[i].price);
        
        lista.append(elem);
    }
    
    $(".products").html(lista);
}


//Mostrar con botonones las opciones disponibles para ordenar la lista de resultados
function listarOrdenDisponibles(listOrd){
    
    var botones = $("<div class='botones_container'></div>");
    for (var i = 0; i < listOrd.length; i++) {
        
        var boton = $("<input/>").attr("value", listOrd[i].name ).attr("type", "button");
        
        var param = '{"sort":"' + listOrd[i].id + '"}';
        
        //Se ultiliza este metodo (.bind) ya que crear el string no me permitia enviarle un JSON al HTML
        boton.bind("click", {sort: listOrd[i].id}, function( event ) {
            
            buscarConjQuery( event.data );
        });
        
        botones.append(boton);
    
    }
    
    $("#botonesOrden").html(botones);

}


//Mostrar con checkbox las opciones disponibles para filtrar la lista de resultados
function listarFiltrosDisponibles(listFil, listApl){
    
    var parentDiv = $("div.filters").html("");
    
    
    for (var i = 0; i < listFil.length; i++) {
        
        var filterDiv = $( "<div></>" ).append( listFil[i].name ).addClass( listFil[i].id );

        for (var j = 0; j < listFil[i].values.length; j++) {
            
            var paragraph = $("<p></p>");
            //crea boton con el tag input  y sus atributos
            var radio = $( "<input type='radio'/>" ).attr( "name",listFil[i].id ).attr( "value", listFil[i].values[j].id );
            paragraph.append(radio).append(listFil[i].values[j].name);
            
            filterDiv.append(paragraph);
        }
        
        parentDiv.append(filterDiv);
        
    }
    
    // imprimo el boton
    parentDiv.append("<input type='button' onclick='filtrar()' value='aplicar_filtros'/>");
    
}

function filtrar() {
    
    console.log( $( "input:checked" ) );
    var result = {};
    // por cada filtro que se activó, se crea un JSON para mandarselo como parametro al metodo buscarConjQuery
    $("input:checked").each(function( index ) {
        
        var name = $(this).attr("name");
        var value = $(this).attr("value");
        
        var ajsonString = '{"' + name + '":"' + value + '"}';
        //Se concate el JSON nuevo con lo que ya tenemos.
        result = $.extend( result, $.parseJSON(ajsonString) );

    });
    //se envia un JSON como parametro
    buscarConjQuery( result );
    
}
